from typing import Dict, Optional, Iterator, Tuple, Callable
from abc import abstractmethod, ABC

from overrides import EnforceOverrides
import numpy as np


class BaseAlgorythm(EnforceOverrides, ABC):
    def __init__(self):
        pass

    @abstractmethod
    def main(self, data, *args, **kwargs):
        pass

    def __call__(self, data, *args, **kwargs):
        return self.main(data, *args, **kwargs)
