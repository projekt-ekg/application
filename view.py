import os
import sys

import pandas as pd
from multiprocessing import Process
from PySide2 import QtCore
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtCore import QObject, Slot, Signal, QTimer, QUrl

from model import Model


class MainWindow(QtCore.QObject):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.model = Model()
        self.app = QGuiApplication(sys.argv)
        self.engine = QQmlApplicationEngine()
        # Get context
        self.engine.rootContext().setContextProperty("backend", self)
        # Load QML
        self.engine.load(os.path.join(os.path.dirname(__file__), "qml", "main.qml"))
        # define signals
        # self.get_column = Signal(str)

    @Slot(str)
    def show_output(self, column):
        # hardcoding kwargs for proof of concept
        kwargs = {
            "alg_name": "ExampleAlgorythm",
            "column": column,
            "message": "siema eniu",
        }
        output = self.model.apply_algorythm(**kwargs)
        print(output)

    def main(self):
        # it would not be like this
        self.model.load_data_from_file("./data.csv")

        if not self.engine.rootObjects():
            sys.exit(-1)
        sys.exit(self.app.exec_())


if __name__ == "__main__":
    app_object = MainWindow()
    app_object.main()
