import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import "../" as App


Window {
    width: 1000
    height: 580
    visible: true
    color: "#00232730"
    title: qsTr("Cause")

    Rectangle {
        id: rectangle
        color: "#2c313c"
        anchors.fill: parent
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.bottomMargin: 0
        anchors.topMargin: 0

        Rectangle {
            id: rectangle1
            height: 20
            color: "#21252d"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.rightMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
        }

        Button {
            id: button
            width: 80
            height: 30
            text: qsTr("Show")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            flat: false
            onPressed: {
                backend.show_output(textField.text)
            }
        }

        TextField {
            id: textField
            anchors.top: button.bottom
            anchors.topMargin: 50
            anchors.horizontalCenter: parent.horizontalCenter
            placeholderText: qsTr("Text Field")
        }
    }
    Connections{
        target: backend

//        function onGet_column(stringText){
//            labelTextName.text = stringText
//        }
    }
}
