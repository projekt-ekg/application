from overrides import overrides

import numpy as np
from core import BaseAlgorythm


class ExampleAlgorythm(BaseAlgorythm):
    @overrides
    def main(self, data, column, message=None):
        """
        Example algorythm that does nothing

        Args:
            data (np.ndarray | pd.DataFrame): data needs to be a
                first argument
            column (string): whatever you want to have here
            message (string, optional): [description]. whatever
                you want to have here

        Returns:
            scalar(np.float): mean from sinusoid, advanced 
                approximation of zero
        """
        data = data[column]
        if message:
            print(f"[message from ExampleAlgorythm]: {message}")
        return np.mean(data)
