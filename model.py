import pandas as pd

from algorythms import ExampleAlgorythm

# all imported algorythms need to be in this dictionary
# to be callable in Model.apply_algorythm()
ALGS = {"ExampleAlgorythm": ExampleAlgorythm}


class Model:
    def __init__(self):
        self.df = None
        pass

    def load_data_from_file(self, path: str):
        self.df = pd.read_csv(path)

    def apply_algorythm(self, alg_name, *alg_args, **alg_kwargs):
        """
        Takes algorythm name, uses it by accessing ALGS, and send alg_args and
        alg kwargs to it.
        """
        alg = ALGS[alg_name]()
        output = alg(self.df, *alg_args, **alg_kwargs)
        return output

    def save_data(self):
        pass
