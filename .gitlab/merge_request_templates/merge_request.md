### Podsumowanie

Tu umieść krótkie podsumowanie merge requesta - co zostaje wprowadzone, jakie zmiany są łamiące itd. Ewentualnie link do issue jakiego dotyczy

### Dependencies
Tu wpisz jeśli twój merge zależy od innego i podaj jego link.

### Task list
- [ ] Kod dodany w odpowiednim miejscu (np. algorythms/init.py)
- [ ] Sprawdzenie czy dodany kod działa z obecnym programem (np. poprzez algorythm_testing.py)
- [ ] Update dodanego kodu we wszystkich miejscach docelowych (np. do słownika ALGS w model.py)
- [ ] Kod odczytuje i zapisuje dane w odpowiednich zmiennych w programie
- [ ] Wykonany rebase kodu `checkout testing -> pull -> checkout your-branch -> rebase testing`
- [ ] Dodana dokumentacja kodu oraz nieoczywistych jego fragmentów


### Review
- [ ] Kod czysty, spełniający wymagania opisane w instrukcji nazewnictwa
- [ ] Dodana dokumentacja w kodzie, a jeśli trzeba to na dysku
- [ ] Squash commit zaznaczony, commit odpowiednio opisany
- [ ] Kod działa z resztą aplikacji i działa zgodnie z wymaganiami
